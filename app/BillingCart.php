<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BillingCart extends Model {
    protected $table = 'billing_cart';

    /**
     * Get the post's cart details.
     *
     * @return array(BillingCartDetail)
     */
    public function cartDetails() {
        return $this->hasMany(BillingCartDetail::class, 'cart_id');
    }

    /**
     * Get the post's customer.
     *
     * @return Customer
     */
    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    /**
     * Get the post's shipper.
     *
     * @return Shipper
     */
    public function shipper() {
        return $this->belongsTo(Shipper::class, 'shipper_id');
    }

    /**
     * Get the post's price.
     *
     * @return int
     */

    public function price() {
        $sales = DB::table('billing_cart_detail')
            ->select(DB::raw('sum(billing_cart_detail.price*billing_cart_detail.quantity) AS total_sales'))
            ->where('billing_cart_detail.cart_id', $this->id)
            ->first();

        if (empty($sales)) {
            return 0;
        }

        return $sales->total_sales;
    }

    public function getState() {
        switch ($this->state) {
            case 'draft':
                return 'Chưa xác nhận';
                break;
            case 'confirmed':
                return 'Đã xác nhận';
                break;
            case 'assigned':
                return 'Đã vận chuyển';
                break;
            case 'received':
                return 'Đã nhận';
                break;
            case 'paid':
                return 'Đã thanh toán';
                break;
            case 'cancel':
                return 'Đã hủy';
                break;
            default:
                return 'Không rõ';
                break;
        }
    }

    public function isDraft() {
        return $this->state == 'draft';
    }

    public function isConfirmed() {
        return $this->state == 'confirmed';
    }

    public function isAssigned() {
        return $this->state == 'assigned';
    }

    public function isReceived() {
        return $this->state == 'received';
    }

    public function isPaid() {
        return $this->state == 'paid';
    }

    public function isCancel() {
        return $this->state == 'cancel';
    }
}