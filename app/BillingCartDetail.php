<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingCartDetail extends Model {
    protected $table = 'billing_cart_detail';

    /**
     * Get the post's billing_cart.
     *
     * @return BillingCart
     */
    public function billingCart() {
        return $this->belongsTo(BillingCart::class, 'cart_id');
    }

    /**
     * Get the post's product.
     *
     * @return Product
     */
    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    /**
     * Get the post's status.
     *
     * @return String
     */

    public function getState() {
        switch ($this->state) {
            case 'completed':
                return "Mua";
            default:
                return "Không lấy";
        }
    }
}