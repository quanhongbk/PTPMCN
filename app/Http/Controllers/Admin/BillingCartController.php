<?php

namespace App\Http\Controllers\Admin;

use App\BillingCart;
use App\BillingCartDetail;
use App\Http\Controllers\AdminController;
use App\Shipper;
use Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BillingCartController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'product');
    }

    /*
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 5);

        $count = BillingCart::get()->count();
        $pageNumber = ceil($count / $limit);

        $billingCarts = BillingCart::take($limit)->offset($limit * $page - $limit)->get();
        return view('admin.billing_cart.index', compact('billingCarts', 'pageNumber', 'page'));
    }

    /**
     * Show the form for detail the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $cart = BillingCart::where('id', $id)->first();
        $shippers = Shipper::all();
        if ($cart) {
            return view('admin.billing_cart.detail', compact('cart', 'shippers'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for detail the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($cartId)
    {
        $cart = BillingCart::where('id', $cartId)->first();
        $shippers = Shipper::all();
        if ($cart) {
            return view('admin.billing_cart.edit', compact('cart', 'shippers'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($cartId, Request $request)
    {
        $cart = BillingCart::find($cartId);

        $customer_name = $request->input('customer_name');
        $customer_email = $request->input('customer_email');
        $customer_address = $request->input('customer_address');
        $customer_phone = $request->input('customer_phone');

        $customer = $cart->customer;
        $customer->name = $customer_name;
        $customer->email = $customer_email;
        $customer->address = $customer_address;
        $customer->phone = $customer_phone;
        $customer->updated_at = Carbon::now();

        if (!$customer->save()) {
            $request->session()->flash('alert-danger', 'Cập nhật thông tin khách hàng xảy ra lỗi');
            return redirect('order/' . $cart->id.'/edit');
        }

        $state = $request->input('state');
        $valided_at = $request->input('valided_at');

        $cart->valided_at = date_format(date_create_from_format('d/m/Y h:i', $valided_at), 'Y-m-d h:i:s');

        if (!empty($state)) {
            $cart->state = $state;
        }

        $shipper_id = $request->input('shipper_id');
        $cart->shipper_id = $shipper_id;

        if ($cart->update()) {
            $request->session()->flash('alert-success', 'Cập nhật thành công');
            return redirect('order/' . $cart->id);
        } else {
            $request->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('order/' . $cart->id.'/edit');
        }
    }

    public function updateState($cartId, Request $request) {
        $cart = BillingCart::find($cartId);

        $state = $request->input('state');

        $cart->state = $state;

        if ($cart->update()) {
            $request->session()->flash('alert-success', 'Cập nhật thành công');
        } else {
            $request->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('order/' . $cart->id.'/edit');
        }

        return redirect('order/' . $cart->id);

    }

    public function removeProduct($cartId, $productId) {
        $cartDetail = BillingCartDetail::where('cart_id', $cartId)->where('product_id', $productId)->delete();

        if (!empty($cartDetail)) {

        }

        return redirect('order/' . $cartId);
    }

}
