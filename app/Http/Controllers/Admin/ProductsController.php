<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ProductRequest;
use App\Product;
use App\ProductImage;
use App\Trademark;
use App\Http\Requests\Admin\ArticleRequest;
use Datatables;
use Illuminate\Http\Request;

class ProductsController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'product');
    }

    /*
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        // Show the page
        $search = $request->input('search', '');
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 5);


        if (empty($search)) {
            $products = Product::take($limit)->offset($page * $limit - $limit)->get();
            $count = Product::get()->count();
        } else {
            $products = Product::where('name', 'LIKE', '%'.$search.'%')->orWhere('unit', 'LIKE', '%'.$search.'%')->take($limit)->offset($page * $limit - $limit)->get();
            $count = Product::where('name', 'LIKE', '%'.$search.'%')->orWhere('unit', 'LIKE', '%'.$search.'%')->count();
        }

        $pageNumber = ceil($count / $limit);
        return view('admin.products.index', compact('products', 'page', 'pageNumber', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $trademarks = Trademark::all();
        return view('admin.products.create', compact('trademarks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $productRequest)
    {
        $product = new Product();
        $product->name = $productRequest->name;
        $product->unit = $productRequest->unit;
        $product->price = $productRequest->price;
        $product->trademark_id = $productRequest->trademark_id;
        $product->p_line = $productRequest->p_line;
        $product->machine_type = $productRequest->machine_type;
        $product->size = $productRequest->size;
        $product->shell_material = $productRequest->shell_material;
        $product->hardness_tolerance = $productRequest->hardness_tolerance;
        $product->quantity = $productRequest->quantity;
        $product->sex = $productRequest->sex;

        if ($product->save()) {
            $productRequest->session()->flash('alert-success', 'Thêm mới ' . $product->name . ' thành công');
            return redirect('product/' . $product->id);
        } else {
            $productRequest->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('product/create');
        }
    }

    /**
     * Show the form for detail the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->first();

        if ($product) {
            return view('admin.products.detail', compact('product'));
        } else {
            abort(404);
        }


    }

    public function addImg($productId, Request $request)
    {

        $product = Product::where('id', $productId)->first();

        if (empty($product)) {
            $request->session()->flash('alert-danger', 'Sản phẩm không tồn tại');
            return redirect('product');
        }

        if ($request->hasFile('imageFile')) {
            $file = $request->file('imageFile');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $fileName = '/image/products/' . $productId . '/' . date('Y/m/d');
            $destinationPath = public_path() . $fileName;

            try {
                $request->file('imageFile')->move($destinationPath, $picture);

                $productImage = new ProductImage();
                $productImage->path = $fileName.'/'.$picture;
                $productImage->product_id = $productId;

                if ($productImage->save()) {
                    $request->session()->flash('alert-success', 'Thêm ảnh thành công');
                } else {
                    $request->session()->flash('alert-danger', 'Có lỗi xảy ra với cơ sở dữ liệu');
                }

                return redirect('product/' . $productId);

            } catch (Exception $e) {
                $request->session()->flash('alert-danger', 'Không rõ lỗi');
                return redirect('product/' . $productId);
            }



        } else {
            $request->session()->flash('alert-danger', 'Bạn vui lòng chọn ảnh');
            return redirect('product/' . $productId);
        }
    }

    public function removeImg($productId, $imageId)
    {
        $product = Product::where('id', $productId)->first();

        if (empty($product)) {
            Session()->flash('alert-danger', 'Sản phẩm không tồn tại');
        }

        $product_image = ProductImage::where('id', $imageId)->first();

        if (empty($product_image)) {
            Session()->flash('alert-danger', 'File không tồn tại');
        }

        $product_image->delete();

        return redirect('product/'.$productId);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($productId)
    {
        $product = Product::find($productId);
        $trademarks = Trademark::all();
        return view('admin.products.edit', compact('product', 'trademarks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($productId, ProductRequest $productRequest)
    {
        $product = Product::find($productId);
        $product->name = $productRequest->name;
        $product->unit = $productRequest->unit;
        $product->price = $productRequest->price;
        $product->trademark_id = $productRequest->trademark_id;
        $product->p_line = $productRequest->p_line;
        $product->machine_type = $productRequest->machine_type;
        $product->size = $productRequest->size;
        $product->shell_material = $productRequest->shell_material;
        $product->hardness_tolerance = $productRequest->hardness_tolerance;
        $product->quantity = $productRequest->quantity;
        $product->sex = $productRequest->sex;

        if ($product->update()) {
            $productRequest->session()->flash('alert-success', 'Cập nhật thành công');
            return redirect('product/' . $product->id);
        } else {
            $productRequest->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('product/'.$product->id.'/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */

    public function delete(Article $article)
    {
        return view('admin.article.delete', compact('article'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
    }


    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $articles = Article::with('category', 'language')
            ->get()
            ->map(function ($article) {
                return [
                    'id' => $article->id,
                    'title' => $article->title,
                    'category' => isset($article->category) ? $article->category->title : "",
                    'language' => isset($article->language) ? $article->language->name : "",
                    'created_at' => $article->created_at->format('d.m.Y.'),
                ];
            });

        $d = Datatables::of($articles)
            ->add_column('actions', '<a href="{{{ url(\'admin/article/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ url(\'admin/article/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                    <input type="hidden" name="row" value="{{$id}}" id="row">')
            ->remove_column('id')
            ->make();
        dd($d);


        return Datatables::of($articles)
            ->add_column('actions', '<a href="{{{ url(\'admin/article/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ trans("admin/modal.edit") }}</a>
                    <a href="{{{ url(\'admin/article/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ trans("admin/modal.delete") }}</a>
                    <input type="hidden" name="row" value="{{$id}}" id="row">')
            ->remove_column('id')
            ->make();
    }
}
