<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\ProductRequest;
use App\Shipper;
use App\Trademark;
use Illuminate\Http\Request;
class ShipperController extends AdminController
{

    public function __construct()
    {
        view()->share('type', 'product');
    }

    /*
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        // Show the page
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 5);
        $shipper = Shipper::take($limit)->offset($limit * $page - $limit)->get();

        $count = Shipper::get()->count();
        $pageNumber = ceil($count / $limit);
        return view('admin.shipper.index', compact('shipper', 'page', 'pageNumber'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.shipper.create', compact('trademarks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $shipper = new Shipper();
        $shipper->name = $request->name;
        $shipper->address = $request->address;
        $shipper->birthday = date_format(date_create_from_format('d/m/Y', $request->birthday), 'Y-m-d');
        $shipper->phone = $request->phone;
        $shipper->email = $request->email;
        $shipper->sex = $request->sex;
        $shipper->status = $request->status;

        if ($request->hasFile('imageFile')) {
            $file = $request->file('imageFile');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            $fileName = '/image/shipper/' . $shipper->name . '/' . date('Y/m/d');
            $destinationPath = public_path() . $fileName;

            try {
                $request->file('imageFile')->move($destinationPath, $picture);
                $shipper->avatar = $fileName.'/'.$picture;
            } catch (Exception $e) {
                $request->session()->flash('alert-danger', 'Không rõ lỗi');
                return redirect('shipper/create');
            }
        }

        if ($shipper->save()) {
            $request->session()->flash('alert-success', 'Thêm mới ' . $shipper->name . ' thành công');
            return redirect('shipper/' . $shipper->id);
        } else {
            $request->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('shipper/create');
        }
    }

    /**
     * Show the form for detail the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $shipper = Shipper::where('id', $id)->first();

        if ($shipper) {
            return view('admin.shipper.detail', compact('shipper'));
        } else {
            abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($shipperId)
    {
        $shipper = Shipper::where('id', $shipperId)->first();
        if (empty($shipper)) {
            abort(404);
        }

        return view('admin.shipper.edit', compact('shipper'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($shipperId, Request $request)
    {
        $shipper = Shipper::where('id', $shipperId)->first();
        if (empty($shipper)) {
            $request->session()->flash('alert-danger', 'Nhân viên không tồn tại');
            return redirect('shipper/'.$shipper->id.'/edit');
        }

        $shipper->name = $request->name;
        $shipper->address = $request->address;
        $shipper->birthday = date("Y-m-d", strtotime($request->birthday));;

        $shipper->phone = $request->phone;
        $shipper->email = $request->email;
        $shipper->sex = $request->sex;
        $shipper->status = $request->status;

        if ($shipper->update()) {
            $request->session()->flash('alert-success', 'Cập nhật thành công');
            return redirect('shipper/' . $shipper->id);
        } else {
            $request->session()->flash('alert-danger', 'Cố lỗi xảy ra với cơ sở dữ liệu');
            return redirect('shipper/'.$shipper->id.'/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */

    public function delete(Article $article)
    {
        return view('admin.article.delete', compact('article'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
    }
}
