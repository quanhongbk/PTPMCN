<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 12/4/17
 * Time: 12:05 AM
 */

namespace App\Http\Controllers\Api;


class ApiResponse
{
    private $status;
    private $msg;
    private $data;

    function __construct($status, $msg, $data) {
        $this->data = $data;
        $this->msg = $msg;
        $this->status = $status;
    }

    function echoJSON() {
        echo(json_encode([
            'status' => $this->status,
            'msg' => $this->msg,
            'data' => $this->data]));
    }

}