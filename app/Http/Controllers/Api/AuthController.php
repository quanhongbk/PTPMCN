<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 11/30/17
 * Time: 2:40 AM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['admin'] = 2;
        $user = User::create($input);
        return (new ApiResponse(200, 'Thành công', ['user' => $user]))->echoJSON();
    }

    public function login(Request $request)
    {
        $input = $request->all();

        if (!$token = JWTAuth::attempt($input)) {
            return (new ApiResponse(0, 'wrong email or password.', 0))->echoJSON();
        }

        $user = JWTAuth::toUser($token);
        $data = [
            'user' => [
                'username' => $user['username'],
                'name'     => $user['name'],
                'email'    => $user['email'],
            ],

            'token' => $token
        ];

        return (new ApiResponse(200, 'Thành công', $data))->echoJSON();
    }

    public function get_user_details(Request $request)
    {
        $input = $request->all();
        $user = JWTAuth::toUser($input['token']);
        return response()->json(['result' => $user]);
    }
}