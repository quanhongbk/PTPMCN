<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 11/28/17
 * Time: 1:29 AM
 */


namespace App\Http\Controllers\Api;

use App\Shipper;
use Carbon\Carbon;
use JWTAuth;
use App\BillingCart;
use App\BillingCartDetail;
use App\Customer;
use App\Http\Controllers\Controller;
use App\ProductImage;
use App\Trademark;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;

class BillingCartController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 10);


        $user = JWTAuth::toUser($request->token);

        if ($user->isUser()) {
            $carts = BillingCart::join('customer', 'customer.id', '=', 'customer_id')->where('customer.user_id', '=', $user->id)->get();

            $data = array();
            foreach ($carts as $cart) {
                array_push($data, [
                   'id' => $cart->id,
                   'valided_at' => empty($cart->valided_at) ? "" : $cart->valided_at,
                   'state' => $cart->state,
                   'price' => empty($cart->price()) ? 0 : $cart->price(),
                ]);
            }

            return (new ApiResponse(200, 'Thành công', $data))->echoJSON();
        }

        /*
        $shipper = Shipper::where('user_id', $user->id);

        $carts = BillingCart::where('shipper_id', $shipper->id)take($limit)->offset($page * $limit - $limit)->get();

        $data = array();
        foreach ($carts as $c) {
            $customer = $c->customer;

            $cjson = [
                'customer' => [
                    'name' => $customer->name,
                    'email' => $customer->email,
                    'phone' => $customer->phone,
                    'address' => $customer->address,
                ],
                'id' => $c->id,
                'valided_at' => $c->valided_at,
                'state' => $c->state,
                'price' => $c->price()
            ];

            array_push($data, $cjson);

        }
        */

        $data = array();
        return (new ApiResponse(200, 'Thành công', $data))->echoJSON();
    }

    public function store(Request $request)
    {
        $products = json_decode($request->input('products'));

        if (empty($products) || count($products) == 0) {
            return (new ApiResponse(0, 'Vui lòng chọn sản phẩm.', []))->echoJSON();
        }

        $name = $request->input('name');
        if (empty($name)) {
            return (new ApiResponse(0, 'Vui lòng nhập tên khách hàng.', []))->echoJSON();
        }

        $phone = $request->input('phone');
        if (empty($phone)) {
            return (new ApiResponse(0, 'Vui lòng nhập số điện thoại.', []))->echoJSON();
        }

        $email = $request->input('email');
        if (empty($email)) {
            return (new ApiResponse(0, 'Vui lòng nhập email.', []))->echoJSON();
        }

        $address = $request->input('address');
        if (empty($address)) {
            return (new ApiResponse(0, 'Vui lòng nhập địa chỉ.', []))->echoJSON();
        }

        $user = JWTAuth::toUser($request->token);

        $customer = Customer::where('phone', $phone)->where('user_id', $user->id)->first();
        if (empty($customer)) {
            $customer = new Customer();
            $customer->name = $name;
            $customer->phone = $phone;
            $customer->email = $email;
            $customer->address = $address;

            $customer->user_id = $user->id;

            if (!$customer->save()) {
                return (new ApiResponse(0, 'Lỗi khi lưu khách hàng.', []))->echoJSON();
            }
        }

        DB::beginTransaction();

        $success = false;

        try {
            $billing_cart = new BillingCart();
            $billing_cart->customer_id = $customer->id;
            $billing_cart->ordered_at = Carbon::now();
            $billing_cart->status = 'enable';
            $billing_cart->state = 'draft';
            $billing_cart->save();

            foreach ($products as $p) {
                $quantity = $p->quantity;
                $p = Product::where('id', '=', $p->product_id)->first();
                if (empty($p)) {
                    DB::rollback();
                } else {
                    $cartDetail = new BillingCartDetail();
                    $cartDetail->cart_id = $billing_cart->id;
                    $cartDetail->product_id = $p->id;
                    $cartDetail->quantity = $quantity;

                    $cartDetail->price = $p->price;
                    $cartDetail->status = 'enable';
                    $cartDetail->save();

                    $success = true;
                }

            }

        } catch (\Exception $e) {
            //failed logic here
            DB::rollback();
            throw $e;
        }

        DB::commit();

        if ($success) {
            return (new ApiResponse(200, 'Bạn đã đặt hàng thành công.', []))->echoJSON();
        }

    }

}