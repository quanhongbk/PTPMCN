<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 11/28/17
 * Time: 1:29 AM
 */


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\ProductImage;
use App\Trademark;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 10);

        $sex = $request->input('sex');
        if (!empty($sex) && in_array($sex, ['male', 'female'])) {
            $products = Product::select('id', 'name', 'unit', 'price', 'trademark_id', 'sex')->where('sex', $sex)->take($limit)->offset($page * $limit - $limit)->get();
        } else {
            $products = Product::select('id', 'name', 'unit', 'price', 'trademark_id', 'sex')->take($limit)->offset($page * $limit - $limit)->get();
        }

        $data = array();
        foreach ($products as $p) {
            // Lấy ảnh
            $images = ProductImage::where('product_id', '=', $p['id'])->get();
            $paths = array();
            foreach ($images as $i) {
                array_push($paths, $i['path']);
            }

            // Lấy thương hiệu
            $trademark = Trademark::where('id', '=', $p['trademark_id'])->first();
            $pr = [
                'id' => $p['id'],
                'name' => $p['name'],
                'price' => $p['price'],
                'sex' => $p['sex'],
                'images' => $paths,
            ];

            if (!empty($trademark)) {
                $pr['trademark'] = [
                    'id' => $trademark['id'],
                    'name' => $trademark['name'],
                ];
            }

            array_push($data, $pr);
        }

        return (new ApiResponse(200, 'Thành công', $data))->echoJSON();
    }

    public function show(Request $request, $productId)
    {
        $product = Product::select('id', 'name', 'unit', 'price', 'trademark_id', 'xuatxu', 'p_line', 'machine_type', 'size', 'shell_material', 'hardness_tolerance', 'quantity')->where('id', '=', $productId)->first();

        $images = ProductImage::where('product_id', '=', $product['id'])->get();
        $paths = array();
        foreach ($images as $i) {
            array_push($paths, $i['path']);
        }

        // Lấy thương hiệu
        $trademark = Trademark::where('id', '=', $product['trademark_id'])->first();

        $p = [
            'id' => $product['id'],
            'name' => $product['name'],
            'price' => $product['price'],
            'xuatxu' => $product['xuatxu'],
            'p_line' => $product['p_line'],
            'machine_type' => $product['machine_type'],
            'size' => $product['size'],
            'shell_material' => $product['shell_material'],
            'hardness_tolerance' => $product['hardness_tolerance'],
            'quantity' => $product['quantity'],
            'sex' => $product['sex'],
            'images' => $paths,
        ];

        if (!empty($trademark)) {
            $p['trademark'] = [
                'id' => $trademark['id'],
                'name' => $trademark['name'],
            ];
        }

        return (new ApiResponse(200, 'Thành công', $p))->echoJSON();
    }


}