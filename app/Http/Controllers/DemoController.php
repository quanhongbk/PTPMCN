<?php

namespace App\Http\Controllers;

use DB;
use App\Demo;

class DemoController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$demo = Demo::all();
		return view('admin.demo.index', compact('demo'));
	}

}