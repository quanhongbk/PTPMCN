<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 11/27/17
 * Time: 1:34 PM
 */
Route::group([], function() {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::group(['middleware' => 'jwt-auth'], function () {
        Route::post('get_user_details', 'APIController@get_user_details');
    });
});

Route::group(['middleware' => ['jwt-auth', 'cors']], function() {
    //Product
    Route::get('product', 'ProductController@index');
    Route::get('product/{id}', 'ProductController@show');

    //Order
    Route::post('order', 'BillingCartController@store');
    Route::get('order', 'BillingCartController@index');
});
