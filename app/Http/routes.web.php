<?php

/****************   Model binding into route **************************/
Route::model('language', 'App\Language');
Route::model('user', 'App\User');
Route::pattern('id', '[0-9]+');
Route::pattern('slug', '[0-9a-z-_]+');

/***************    Site routes  **********************************/
/*
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('about', 'PagesController@about');
Route::get('demo',  'DemoController@index');
Route::get('contact', 'PagesController@contact');
Route::get('articles', 'ArticlesController@index');
Route::get('article/{slug}', 'ArticlesController@show');
Route::get('video/{id}', 'VideoController@show');
Route::get('photo/{id}', 'PhotoController@show');
*/

Route::get('auth/login', function () {
    return view('auth.login');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/***************    Admin routes  **********************************/
Route::group(['middleware' => 'admin'], function() {
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('home', 'Admin\DashboardController@index');
    # Admin Dashboard
    Route::get('dashboard', 'Admin\DashboardController@index');
    # Language
    Route::get('language/data', 'Admin\LanguageController@data');
    Route::get('language/{language}/show', 'Admin\LanguageController@show');
    Route::get('language/{language}/edit', 'Admin\LanguageController@edit');
    Route::get('language/{language}/delete', 'Admin\LanguageController@delete');
    Route::resource('language', 'Admin\LanguageController');

    # Users
    Route::get('user/data', 'Admin\UserController@data');
    Route::get('user/{user}/show', 'Admin\UserController@show');
    Route::get('user/{user}/edit', 'Admin\UserController@edit');
    Route::get('user/{user}/delete', 'Admin\UserController@delete');
    Route::resource('user', 'Admin\UserController');

    #Product
    Route::get('product/{product}/edit', 'Admin\ProductsController@edit');
    Route::post('product/{product}/addImg', 'Admin\ProductsController@addImg');
    Route::get('product/{product}/remove/{image}', 'Admin\ProductsController@removeImg');
    Route::get('product/{product}/delete', 'Admin\ProductsController@delete');
    Route::resource('product', 'Admin\ProductsController');

    #Shipper
    Route::resource('shipper', 'Admin\ShipperController');

    #Billing Cart
    Route::resource('order', 'Admin\BillingCartController');
    Route::get('order/completed/{cart}', 'Admin\BillingCartController@updateState');
    Route::get('order/{cart}/removeProduct/{productId}', 'Admin\BillingCartController@removeProduct');
    #Message
    Route::get('sendMsg', 'Admin\SendMessageController@sendMsg');
});
