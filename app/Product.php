<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $table = 'product';

    /**
     * Get the post's trademark.
     *
     * @return Trademark
     */
    public function trademark()
    {
        return $this->belongsTo(Trademark::class, 'trademark_id');
    }

    /**
     * Get the post's images.
     *
     * @return ProductImage
     */
    public function images() {
        return $this->hasMany(ProductImage::class);
    }
}