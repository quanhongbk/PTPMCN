<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {
    protected $table = 'product_image';

    /**
     * Get the post's product.
     *
     * @return Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}