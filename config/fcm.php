<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAARAfOwFw:APA91bFEd75NfGqqg4UKwKQ6mMT4gELI6glZJ1tDW05s45ear4ltz145uD8SANB2GiwwSrxR3cU5YC2NC7x2LsFYt50KWvOd_g-TTcsT08XUtw7tTkRHIn7NvS85HAmAAz-mz3b1VjK9'),
        'sender_id' => env('FCM_SENDER_ID', 'AIzaSyBLptHrvcyz8MBnuBXBxcfS2IdKdBLaOzk'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
