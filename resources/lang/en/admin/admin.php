<?php

return [
	'settings' => 'Settings',
	'homepage' => 'View Homepage',
	'home' => 'Home',
	'welcome' => 'Welcome',
    'action' => 'Actions',
    'back' => 'Quay lại',
    'save' => 'Lưu',
    'edit' => 'Sửa',
    'created_at' => 'Created at',
    'language' => 'Language',
    'yes'       => 'Yes',
    'no'        =>  'No',
    'view_detail' => 'View Details',
    'news_categories' => 'Article categories',
    'news_items' => 'Article items',
    'photo_albums' => 'Photo albums',
    'photo_items' => 'Photo items',
    'users' => 'Users',
	];