@extends('admin.layouts.default')
{{-- Content --}}
@section('main')
    <!-- Tabs -->
    <div class="page-header">
        <h4>
            {!! trans("admin/product.product") !!} / Thêm mới
            <div class="pull-right">
                <a href="{!!  url('/product') !!}"
                   class="btn btn-sm  btn-default iframe"><span
                            class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                <a href="#"
                   class="btn btn-sm  btn-primary iframe" onclick="submitForm()"><span
                            class="glyphicon glyphicon-save"></span> {!! trans('admin/admin.save')!!} </a>
            </div>
        </h4>
    </div>

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>

    <form id="create_form" method="post" action= {!! url('/product') !!}>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="tab-content">
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-heading"><b>Thông tin sản phẩm</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Tên</label>
                                        <input class="form-control" id="name" name="name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Đơn giá</label>
                                        <input type="number" class="form-control" id="price" name="price">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="trademark_id">Thương hiệu</label>
                                        <select class="form-control" id="trademark_id" name="trademark_id">
                                            @foreach($trademarks as $trademark)
                                                <option value="{!! $trademark['id'] !!}">{!! $trademark['name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="p_line">Dòng sản phẩm</label>
                                        <input class="form-control" id="p_line" name="p_line">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="machine_type">Kiểu máy</label>
                                        <input class="form-control" id="machine_type" name="machine_type">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="size">Kích cỡ</label>
                                        <input class="form-control" id="size" name="size">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="hardness_tolerance">Độ chịu cứng</label>
                                        <input class="form-control" id="hardness_tolerance" name="hardness_tolerance">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="shell_material">Chất liệu vỏ</label>
                                        <input class="form-control" id="shell_material" name="shell_material">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Miêu tả khác</label>
                                        <textarea class="form-control" id="usr"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-heading"><b>Khác</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="unit">Đơn vị</label>
                                        <input class="form-control" id="unit" name="unit">
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">Số lượng</label>
                                        <input type="number" class="form-control" id="quantity" name="quantity">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--
<div class="form-group">
    <div class="col-md-12">
        <button type="reset" class="btn btn-sm btn-warning close_popup">
            <span class="glyphicon glyphicon-ban-circle"></span> {{
						trans("admin/modal.cancel") }}
            </button>
            <button type="reset" class="btn btn-sm btn-default">
                <span class="glyphicon glyphicon-remove-circle"></span> {{
						trans("admin/modal.reset") }}
            </button>
            <button type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span>
@if	(isset($news))
        {{ trans("admin/modal.edit") }}
    @else
        {{trans("admin/modal.create") }}
    @endif
            </button>
        </div>
    </div>
    <!-- ./ form actions -->

    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        function submitForm() {
            $('#create_form').submit();
        }
    </script>
@endsection
