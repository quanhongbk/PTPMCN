@extends('admin.layouts.default')
{{-- Content --}}
@section('styles')
@endsection
@section('main')
    <!-- Tabs -->
    <div class="tab-content">
        <div class="page-header">
            <h4>
                Hóa đơn / {{ $cart['id'] }}
                <div class="pull-right">
                    <a href="{!!  url('/product') !!}"
                       class="btn btn-sm  btn-default iframe"><span
                                class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                    @if($cart->isDraft())
                        <a href="{!!  url('/order/completed/'.$cart->id.'?state=confirmed') !!}"
                           class="btn btn-sm  btn-primary iframe"><span
                                    class="glyphicon glyphicon-backward"></span> Xác nhận đơn hàng </a>
                        <a href="{!!  url('/product') !!}"
                           class="btn btn-sm  btn-danger iframe"><span
                                    class="glyphicon glyphicon-backward"></span> Hủy </a>
                    @endif

                    @if($cart->isConfirmed())
                        <a href="{!!  url('/order/completed/'.$cart->id.'?state=assigned') !!}"
                           class="btn btn-sm  btn-primary iframe"><span
                                    class="glyphicon glyphicon-backward"></span> Xác nhận lấy hàng </a>
                        <a href="{!!  url('/product') !!}"
                           class="btn btn-sm  btn-danger iframe"><span
                                    class="glyphicon glyphicon-backward"></span> Hủy </a>
                    @endif

                </div>
            </h4>
        </div>

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                             data-dismiss="alert"
                                                                                             aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>

        <div class="tab-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Thông tin hóa đơn</b><a
                                    class="pull-right" href="{!! url('order/'.$cart['id'].'/edit') !!}"><i
                                        class="fa fa fa-pencil-square-o fa-2x"></i></a></div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Mã hóa đơn</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart['id'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Khách hàng</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart['customer']['name'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Số điện thoại</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart['customer']['phone'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Email</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart['customer']['email'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Địa chỉ</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart['customer']['address'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Nhân viên giao hàng</b></div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Trạng thái</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $cart->getState() }}</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Người giao</b></label>
                                <div class="col-sm-8">
                                    <p>@if($cart->shipper['name']) {!! $cart->shipper['name'] !!} @else Chưa có nhân
                                        viên nào @endif </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Ngày giao</b></label>
                                <div class="col-sm-8">
                                    <p>@if($cart['valided_at']) {!! date('d/m/Y h:i', strtotime($cart['valided_at'])) !!} @else
                                            Chưa có ngày giao @endif </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body">
                            <table id="table" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Thành tiền</th>
                                    <th>Trạng thái</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $index = 0 ?>

                                @foreach ($cart->cartDetails as $detail)
                                    <?php
                                    $index = $index + 1
                                    ?>
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>
                                            <a href="{!! url("product/".$detail->product_id)!!}"><b>{{ $detail->product->name }}</b></a>
                                        </td>
                                        <td>{{ $detail->quantity }}</td>
                                        <td>
                                            <div style="color: green">{{ number_format($detail->price) }} đ</div>
                                        </td>
                                        <td>
                                            <div style="color: red">{{ number_format($detail->price * $detail->quantity) }}
                                                đ
                                            </div>
                                        </td>
                                        <td>{{ $detail->getState() }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle" type="button"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Sửa</a></li>
                                                    <li><a onclick="removeProduct({{ $detail->product_id }})">Xóa</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).on('ready', function () {
        });

        function removeProduct(productId) {
            window.location = "{{ $cart->id }}/removeProduct/" + productId;
        }
    </script>

@endsection
