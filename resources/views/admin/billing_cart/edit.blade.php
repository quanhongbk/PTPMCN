@extends('admin.layouts.default')
{{-- Content --}}

@section('styles')
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
@endsection

@section('main')
    <!-- Tabs -->
    <div class="page-header">
        <h4>
            Đơn hàng / Chỉnh sửa
            <div class="pull-right">
                <a href="{!!  url('/order/'. $cart['id']) !!}"
                   class="btn btn-sm  btn-default iframe"><span
                            class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                <a href="#"
                   class="btn btn-sm  btn-primary iframe" onclick="submitForm()"><span
                            class="glyphicon glyphicon-save"></span> {!! trans('admin/admin.save')!!} </a>
            </div>
        </h4>
    </div>

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>

    <form id="update_form" method="post" action= {!! url('/order/'.$cart['id']) !!}>
        <input type="hidden" name="_method" value="put"/>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="tab-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b>Thông tin đơn hàng</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="customer_name">Khách hàng</label>
                                        <input type="text" class="form-control" id="customer_name" name="customer_name"
                                               value="{{ $cart->customer->name  }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="customer_email">Email</label>
                                        <input type="text" class="form-control" id="customer_email"
                                               name="customer_email" value="{{ $cart->customer->email  }}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="customer_phone">Số điện thoại</label>
                                        <input type="text" class="form-control" id="customer_phone"
                                               name="customer_phone" value="{{ $cart->customer->phone  }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="customer_address">Địa chỉ</label>
                                        <input type="text" class="form-control" id="customer_address"
                                               name="customer_address" value="{{ $cart->customer->address  }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b>Khác</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="status">Trạng thái</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="enable" @if($cart['status'] == 'enable') selected @endif>Có
                                                hiệu lực
                                            </option>
                                            <option value="disable" @if($cart['status'] == 'disable') selected @endif>
                                                Không hiệu lực
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="shipper_id">Nhân viên giao hàng</label>
                                        <select class="form-control" id="shipper_id" name="shipper_id">
                                            @foreach($shippers as $shipper)
                                                <option value="{!! $shipper['id'] !!}" @if($shipper['id'] == $cart['shipper_id']) @endif>{!! $shipper['name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="birthday_id">Ngày giao</label>
                                        <div class='input-group date' id='valided_at'>
                                            <input class="form-control" name="valided_at"/>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--
<div class="form-group">
    <div class="col-md-12">
        <button type="reset" class="btn btn-sm btn-warning close_popup">
            <span class="glyphicon glyphicon-ban-circle"></span> {{
						trans("admin/modal.cancel") }}
            </button>
            <button type="reset" class="btn btn-sm btn-default">
                <span class="glyphicon glyphicon-remove-circle"></span> {{
						trans("admin/modal.reset") }}
            </button>
            <button type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span>
@if	(isset($news))
        {{ trans("admin/modal.edit") }}
    @else
        {{trans("admin/modal.create") }}
    @endif
            </button>
        </div>
    </div>
    <!-- ./ form actions -->

    </form>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        function submitForm() {
            $('#update_form').submit();
        }
        $(function () {
            $('#valided_at').datetimepicker({
                'format': 'DD/MM/YYYY HH:mm'
            });
            $('#valided_at').data("DateTimePicker").date(new Date("{{ $cart['valided_at'] }}"));
        });
    </script>
@endsection
