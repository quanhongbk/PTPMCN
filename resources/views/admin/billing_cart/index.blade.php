@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Hóa đơn :: @parent @endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Hóa đơn
        <!--
            <div class="pull-right">
                <a href="{!!  url('order/create') !!}"
                   class="btn btn-sm  btn-primary iframe"><span
                            class="glyphicon glyphicon-plus-sign"></span> Thêm mới </a>
            </div>
            -->
        </h3>
    </div>

    <div class="row">
        <div class="col-xs-12 col-xs-offset-0">
            <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span id="search_concept">Tìm kiếm</span>
                    </button>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">
                <input type="text" class="form-control" name="x" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span
                                class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>STT</th>
            <th>Mã đơn hàng</th>
            <th>Khách hàng</th>
            <th>Tổng tiền</th>
            <th>Trạng thái</th>
            <th>Cập nhật</th>
        </tr>
        </thead>
        <tbody>
        <?php $index = 0 ?>
        @foreach ($billingCarts as $c)
            <?php
            $index = $index + 1
            ?>
            <tr>
                <td>{{ $index }}</td>
                <td><a href="{!! url("order/".$c['id'])!!}"><b>{!! $c['id'] !!}</b></a></td>
                <td>{{ $c->customer->name }}</td>
                <td>
                    <div style="color: green">{{ number_format($c->price()) }} đ</div>
                </td>
                <td><a>{{ $c->getState() }}</a></td>
                <td>{!! date('m/d/Y', strtotime($c->updated_at)) !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <ul class="pager">
        <?php $prepage = $page - 1 ?>
        <?php $nextpage = $page + 1 ?>
        <li><a href="{{ url('/order?page='.$prepage) }}">Previous</a></li>
        <li>{{ $page }} / {{ $pageNumber }}</li>
        <li><a href= " @if($page < $pageNumber) {{ url('/order?page='.$nextpage) }} @else # @endif ">Next</a></li>
    </ul>
@endsection

{{-- Scripts --}}
@section('scripts')
@endsection
