<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Công ty đồng hồ Hưng Thịnh</a>
    </div>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="{{url('dashboard')}}">
                        <i class="fa fa-dashboard fa-fw"></i> Bảng điều khiển
                    </a>
                </li>
                <li>
                    <a href="{{ url('order') }}">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> Hóa đơn</span>
                    </a>
                    <!--
                    <ul class="nav collapse">
                        <li>
                            <a href="{{url('admin/articlecategory')}}">
                                <i class="glyphicon glyphicon-list"></i>  Nhân viên
                            </a>
                        </li>
                        <li>
                            <a href="{{url('admin/article')}}">
                                <i class="glyphicon glyphicon-bullhorn"></i> Articles
                            </a>
                        </li>
                    </ul>
                    -->
                </li>
                <li>
                    <a href="{{url('product')}}">
                        <i class="fa fa-th" aria-hidden="true"></i> Hàng hóa
                    </a>
                </li>
                <li>
                    <a href="{{ url('shipper') }}">
                        <i class="fa fa-users"></i> Nhân sự
                    </a>
                    <!--
                    <ul class="nav collapse">
                        <li>
                            <a href="{{url('admin/photoalbum')}}">
                                <i class="glyphicon glyphicon-list"></i> Photo albums
                            </a>
                        </li>
                        <li>
                            <a href="{{url('admin/photo')}}">
                                <i class="glyphicon glyphicon-camera"></i> Photo
                            </a>
                        </li>
                    </ul>
                    -->
                </li>
                <li>
                    <a href="{{url('user')}}">
                        <i class="glyphicon glyphicon-user"></i> Tài khoản cá nhân
                    </a>
                </li>
                <li>
                    <a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Đăng xuất</a>
                </li>
            </ul>
        </div>
    </div>
</nav>