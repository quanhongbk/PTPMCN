@extends('admin.layouts.default')
{{-- Content --}}
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/fileinput.min.css') !!}">
@endsection
@section('main')
    <!-- Tabs -->
    <div class="tab-content">
        <div class="page-header">
            <h4>
                {!! trans("admin/product.product") !!} / {{ $product['name'] }}
                <div class="pull-right">
                    <a href="{!!  url('/product') !!}"
                       class="btn btn-sm  btn-default iframe"><span
                                class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                </div>
            </h4>
        </div>

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                             data-dismiss="alert"
                                                                                             aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>

        <div class="tab-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Thông tin sản phẩm</b><a class="pull-right" href="{!! url('product/'.$product['id'].'/edit') !!}"><i
                                        class="fa fa fa-pencil-square-o fa-2x"></i></a></div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Tên</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['name'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Thương hiệu</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['trademark']['name'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Dòng sản phẩm</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['p_line'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Kiểu máy</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['machine_type'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Độ chịu cứng</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['hardness_tolerance'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Chất liệu vỏ</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['shell_material'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Kích cỡ</b></label>
                                <div class="col-sm-8">
                                    <p>{{ $product['size'] }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Giới tính</b></label>
                                <div class="col-sm-8">
                                    <p>@if($product->sex == 'male') Nam @else Nữ @endif</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Ảnh minh họa</b><a class="pull-right"
                                                                                                  data-toggle="modal"
                                                                                                  data-target="#addImageModel"><i
                                        class="fa fa-plus fa-2x" aria-hidden="true"></i></a></div>
                        <div class="panel-body">
                            @if($product['images'] && count($product['images']) > 0)
                                @foreach($product['images'] as $image)
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <img src="{!! url($image['path']) !!}" class="img-rounded" alt="#" width="100%" height="100%">
                                        </div>
                                        <div class="col-sm-2">
                                            <a style="color: black" href="{!! url('product/'.$product['id'].'/remove/'.$image['id']) !!}"><i class="fa fa-times fa-x"></i></a>
                                        </div>
                                    </div>
                                    <br/>
                                @endforeach
                            @else
                                <p>Chưa có ảnh minh họa</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Add Image Modal -->
    <form id="saveImageModel" enctype="multipart/form-data" method="post"
          action="{!! url('product/'.$product['id'].'/addImg') !!}">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div id="addImageModel" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thêm mới ảnh minh họa</h4>
                    </div>
                    <div class="modal-body">
                        <label for="input-folder-2">Upload file ảnh</label>
                        <div class="file-loading">
                            <input id="input-id" type="file" class="file" data-preview-file-type="text"
                                   data-msg-placeholder="Chọn file ảnh" name="imageFile">
                        </div>
                        <div id="errorBlock" class="help-block"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveImage()">Lưu
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{!! asset('js/fileinput.js') !!}" type="text/javascript"></script>

    <script type="text/javascript">
        $("#input-id").fileinput({'showUpload': false, 'previewFileType': 'any'});
        $(document).on('ready', function () {
        });

        function saveImage() {
            $('#saveImageModel').submit();
        }
    </script>
@endsection
