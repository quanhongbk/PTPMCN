@extends('admin.layouts.default')
{{-- Content --}}
@section('main')
    <!-- Tabs -->
    <div class="page-header">
        <h4>
            {!! trans("admin/product.product") !!} / {{ $product['name'] }} /Chỉnh sửa
            <div class="pull-right">
                <a href="{!!  url('/product') !!}"
                   class="btn btn-sm  btn-default iframe"><span
                            class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                <a href="#"
                   class="btn btn-sm  btn-primary iframe" onclick="submitForm()"><span
                            class="glyphicon glyphicon-save"></span> {!! trans('admin/admin.save')!!} </a>
            </div>
        </h4>
    </div>

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>

    <form id="edit_form" method="post" action= {!! url('/product/'.$product['id']) !!}>
        <input type="hidden" name="_method" value="put"/>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="tab-content">
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-heading"><b>Thông tin sản phẩm</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Tên</label>
                                        <input class="form-control" id="name" name="name"
                                               value="{{ $product['name'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="price">Đơn giá</label>
                                        <input type="number" class="form-control currency" id="price_id" name="price"
                                               value="{{ $product['price'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="trademark_id">Thương hiệu</label>
                                        <select class="form-control" id="trademark_id" name="trademark_id">
                                            @foreach($trademarks as $trademark)
                                                <option value="{!! $trademark['id'] !!}"
                                                        @if($product['trademark_id'] == $trademark['id']) selected @endif>{!! $trademark['name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="p_line">Dòng sản phẩm</label>
                                        <input class="form-control" id="p_line" name="p_line"
                                               value="{{ $product['p_line'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="machine_type">Kiểu máy</label>
                                        <input class="form-control" id="machine_type" name="machine_type"
                                               value="{{ $product['machine_type'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="size">Kích cỡ</label>
                                        <input class="form-control" id="size" name="size"
                                               value="{{ $product['size'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="hardness_tolerance">Độ chịu cứng</label>
                                        <input class="form-control" id="hardness_tolerance" name="hardness_tolerance"
                                               value="{{ $product['hardness_tolerance'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="shell_material">Chất liệu vỏ</label>
                                        <input class="form-control" id="shell_material" name="shell_material"
                                               value="{{ $product['shell_material'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Miêu tả khác</label>
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-heading"><b>Khác</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="unit">Đơn vị</label>
                                        <input class="form-control" id="unit" name="unit"
                                               value="{{ $product['unit'] }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">Số lượng</label>
                                        <input type="number" class="form-control" id="quantity" name="quantity"
                                               value="{{ $product['quantity'] }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="quantity">Giới tính</label>
                                        <select class="form-control" id="sex" name="sex">
                                            <option value="male" @if($product['sex'] == 'male') selected @endif>Nam</option>
                                            <option value="female" @if($product['sex'] == 'female') selected @endif>Nữ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--
<div class="form-group">
    <div class="col-md-12">
        <button type="reset" class="btn btn-sm btn-warning close_popup">
            <span class="glyphicon glyphicon-ban-circle"></span> {{
						trans("admin/modal.cancel") }}
            </button>
            <button type="reset" class="btn btn-sm btn-default">
                <span class="glyphicon glyphicon-remove-circle"></span> {{
						trans("admin/modal.reset") }}
            </button>
            <button type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span>
@if	(isset($news))
        {{ trans("admin/modal.edit") }}
    @else
        {{trans("admin/modal.create") }}
    @endif
            </button>
        </div>
    </div>
    <!-- ./ form actions -->

    </form>
@endsection

@section('scripts')
    <script src="{!! asset('js/autoNumeric.js') !!}" type="text/javascript"></script>
    <script type="text/javascript">
        function submitForm() {
            $('#edit_form').submit();
        }

        $(document).ready(function () {
            $("#price_id").autoNumeric('init');
        });

    </script>
@endsection
