@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! trans("admin/product.product") !!} :: @parent @endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            {!! trans("admin/product.product") !!}
            <div class="pull-right">
                <a href="{!!  url('product/create') !!}"
                   class="btn btn-sm  btn-primary iframe"><span
                            class="glyphicon glyphicon-plus-sign"></span> Thêm mới </a>
            </div>
        </h3>
    </div>

    <div class="row">
        <div class="col-xs-12 col-xs-offset-0">
            <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span id="search_concept">Tìm kiếm</span>
                    </button>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">
                <input type="text" class="form-control" name="x" placeholder="Search term..." id="search_input" value="{{ $search }}">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="onSearch()"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Đơn vị</th>
            <th>Đơn giá</th>
            <th>Số luợng</th>
            <th>Cập nhật</th>
        </tr>
        </thead>
        <tbody>
        <?php $index = 0 ?>
        @foreach ($products as $p)
            <?php
            $index = $index + 1
            ?>
            <tr>
                <td>{{ $index }}</td>
                <td><a href="{!! url("product/".$p['id'])!!}"><b>{{ $p['name'] }}</b></a> @ <br/>{{$p['trademark']['name']}}</td>
                <td>{{ $p['unit'] }}</td>
                <td> <div style="color: green">{{ number_format($p['price']) }} đ </div></td>
                <td>{{ number_format($p['quantity']) }}</td>
                <td> 01/01/2017 </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <ul class="pager">
        <?php $prepage = $page - 1 ?>
        <?php $nextpage = $page + 1 ?>
        <li><a href="{{ url('/product?page='.$prepage) }}">Previous</a></li>
        <li>{{ $page }} / {{ $pageNumber }}</li>
        <li><a href= " @if($page < $pageNumber) {{ url('/product?page='.$nextpage) }} @else # @endif ">Next</a></li>
    </ul>
@endsection

{{-- Scripts --}}
@section('scripts')
    <script>
        function onSearch() {
            window.location = '/product?search=' + $('#search_input').val();
        }
    </script>
@endsection
