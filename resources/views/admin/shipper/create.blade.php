@extends('admin.layouts.default')
{{-- Content --}}
@section('styles')
    <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/fileinput.min.css') !!}">
@endsection

@section('main')
    <!-- Tabs -->
    <div class="page-header">
        <h4>
            Nhân viên / Thêm mới
            <div class="pull-right">
                <a href="{!!  url('/product') !!}"
                   class="btn btn-sm  btn-default iframe"><span
                            class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                <a href="#"
                   class="btn btn-sm  btn-primary iframe" onclick="submitForm()"><span
                            class="glyphicon glyphicon-save"></span> {!! trans('admin/admin.save')!!} </a>
            </div>
        </h4>
    </div>

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                         data-dismiss="alert"
                                                                                         aria-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>

    <form id="create_form" method="post" enctype="multipart/form-data" action= {!! url('/shipper') !!}>
        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        <div class="tab-content">
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel">
                        <div class="panel-heading"><b>Thông tin nhân viên</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="name">Tên</label>
                                        <input class="form-control" id="name" name="name">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="gener">Giới tính</label>
                                        <select class="form-control" id="sex" name="sex">
                                            <option value="male">Nam</option>
                                            <option value="female">Nữ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" id="email" name="email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Số điện thoại</label>
                                        <input class="form-control" id="phone" name="phone">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="birthday_id">Ngày sinh</label>
                                        <div class='input-group date' id='birthday_id'>
                                            <input class="form-control" name="birthday"/>
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="address">Địa chỉ</label>
                                        <input class="form-control" id="address" name="address">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Miêu tả khác</label>
                                        <textarea class="form-control" id="usr"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel">
                        <div class="panel-heading"><b>Khác</b></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="unit">Trạng thái</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="enable">Có hiệu lực</option>
                                            <option value="disable">Không hiệu lực</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Upload file avatar</label>
                                        <div class="file-loading">
                                            <input id="input-id" type="file" class="file" data-preview-file-type="text"
                                                   data-msg-placeholder="Chọn file ảnh" name="imageFile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--
<div class="form-group">
    <div class="col-md-12">
        <button type="reset" class="btn btn-sm btn-warning close_popup">
            <span class="glyphicon glyphicon-ban-circle"></span> {{
						trans("admin/modal.cancel") }}
            </button>
            <button type="reset" class="btn btn-sm btn-default">
                <span class="glyphicon glyphicon-remove-circle"></span> {{
						trans("admin/modal.reset") }}
            </button>
            <button type="submit" class="btn btn-sm btn-success">
                <span class="glyphicon glyphicon-ok-circle"></span>
@if	(isset($news))
        {{ trans("admin/modal.edit") }}
    @else
        {{trans("admin/modal.create") }}
    @endif
            </button>
        </div>
    </div>
    <!-- ./ form actions -->

    </form>
@endsection

@section('scripts')
    <script src="{!! asset('js/fileinput.js') !!}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#input-id").fileinput({'showUpload': false, 'previewFileType': 'any'});
        })
        function submitForm() {
            $('#create_form').submit();
        }

        $(function () {
            $('#birthday_id').datetimepicker({
                'format': 'DD/MM/YYYY'
            });
        });
    </script>
@endsection
