@extends('admin.layouts.default')
{{-- Content --}}
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/fileinput.min.css') !!}">
@endsection
@section('main')
    <!-- Tabs -->
    <div class="tab-content">
        <div class="page-header">
            <h4>
                Nhân viên / {{ $shipper['name'] }}
                <div class="pull-right">
                    <a href="{!!  url('/shipper') !!}"
                       class="btn btn-sm  btn-default iframe"><span
                                class="glyphicon glyphicon-backward"></span> {!! trans('admin/admin.back')!!} </a>
                </div>
            </h4>
        </div>

        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                                                                                             data-dismiss="alert"
                                                                                             aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>

        <div class="tab-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Thông tin nhân viên</b><a
                                    class="pull-right" href="{!! url('shipper/'.$shipper['id'].'/edit') !!}"><i
                                        class="fa fa fa-pencil-square-o fa-2x"></i></a></div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <img src="{!! url($shipper['avatar']) !!}" class="img-rounded" alt="#" width="100%"
                                         height="100%">
                                </div>
                                <div class="col-sm-8">
                                    <p><b>{{ $shipper['name'] }}</b> - @if($shipper['sex'] == 'male') Nam @else
                                            Nữ @endif</p>
                                    <p>{{ $shipper['email'] }}</p>
                                    <p>{{ $shipper['phone'] }}</p>
                                    <p>{{ $shipper['address'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel">
                        <div class="panel-heading"><b style="font-size: 15px;">Khác</b></div>
                        <div class="panel-body">
                            <div class="form-group row">
                                <label class="col-sm-4 control-label"><b>Tình trạng</b></label>
                                <div class="col-sm-8">
                                    <div class="col-sm-8">
                                        <p>@if($shipper['status'] == 'enable') Đang hoạt động @else Dừng hoạt động @endif</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('js/fileinput.js') !!}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).on('ready', function () {

        });
    </script>
@endsection
