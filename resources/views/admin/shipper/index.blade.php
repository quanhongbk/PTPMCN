@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Nhân viên :: @parent @endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            Nhân viên

            <div class="pull-right">
                <a href="{!!  url('shipper/create') !!}"
                   class="btn btn-sm  btn-primary iframe"><span
                            class="glyphicon glyphicon-plus-sign"></span> Thêm mới </a>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>STT</th>
            <th>Tên</th>
            <th>Địa chỉ</th>
            <th>Ngày sinh</th>
            <th>Giới tính</th>
            <th>Cập nhật</th>
        </tr>
        </thead>
        <tbody>
        <?php $index = 0 ?>
        @foreach ($shipper as $p)
            <?php
            $index = $index + 1
            ?>
            <tr>
                <td>{{ $index }}</td>
                <td><a href="{!! url("shipper/".$p['id'])!!}"><b>{{ $p['name'] }}</b></a></td>
                <td>{{ $p['address'] }}</td>
                <td> <div>@if($p['birthday']){!! date('d/m/Y', strtotime($p['birthday'])) !!}@endif</div></td>
                <td>{{ number_format($p['quantity']) }}</td>
                <td> 01/01/2017 </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <ul class="pager">
        <?php $prepage = $page - 1 ?>
        <?php $nextpage = $page + 1 ?>
        <li><a href="{{ url('/shipper?page='.$prepage) }}">Previous</a></li>
        <li>{{ $page }} / {{ $pageNumber }}</li>
        <li><a href= " @if($page < $pageNumber) {{ url('/shipper?page='.$nextpage) }} @else # @endif ">Next</a></li>
    </ul>

@endsection

{{-- Scripts --}}
@section('scripts')
@endsection
