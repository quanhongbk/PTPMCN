@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') {!! trans("admin/users.users") !!} :: @parent
@endsection

{{-- Content --}}
@section('main')
    <div class="page-header">
        <h3>
            {!! trans("admin/users.users") !!}
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{!! url('admin/user/create') !!}"
                       class="btn btn-sm  btn-primary iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
					trans("admin/modal.new") }}</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Nhân viên sở hữu</th>
            <th>Email</th>
            <th>Trạng Thái</th>
        </tr>

        <?php $index = 0 ?>
        @foreach($users as $user)
            <?php
            $index += 1
            ?>
            <tr>
                <td></td>
                <td>{{ $user->email }}</td>
                <td>Hoạt động</td>
            </tr>
        @endforeach

        </thead>
        <tbody></tbody>
    </table>
@endsection

{{-- Scripts --}}
@section('scripts')
@endsection
